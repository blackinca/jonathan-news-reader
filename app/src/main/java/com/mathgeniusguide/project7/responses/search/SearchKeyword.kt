package com.mathgeniusguide.project7.responses.search

data class SearchKeyword(
    val major: String,
    val name: String,
    val rank: Int,
    val value: String
)