package com.mathgeniusguide.project7.responses.search

data class SearchMeta(
    val hits: Int,
    val offset: Int,
    val time: Int
)