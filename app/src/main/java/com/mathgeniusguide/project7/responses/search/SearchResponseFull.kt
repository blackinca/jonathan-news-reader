package com.mathgeniusguide.project7.responses.search

data class SearchResponseFull(
    val copyright: String,
    val response: SearchResponse,
    val status: String
)