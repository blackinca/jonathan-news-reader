package com.mathgeniusguide.project7.util

import com.mathgeniusguide.project7.R

object Constants {
    const val API_KEY = "W0yBJTOlR8IoXFBrcyH5w09uWw4DvTvO"
    const val BASE_URL = "https://api.nytimes.com/svc/"

    const val TOP_NEWS = 0
    const val POPULAR_NEWS = 1
    const val POLITICS_NEWS = 2

    const val SEARCH_SCREEN = 0
    const val NOTIFICATION_SCREEN = 1
    const val LOADED_SCREEN = 2
}